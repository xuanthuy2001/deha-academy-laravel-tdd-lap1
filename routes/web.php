<?php

use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/tasks',[TaskController::class, 'index'])->name('task.index')->middleware('auth');
Route::get('tasks/create',[TaskController::class, 'create'])->name('task.create')->middleware('auth');
Route::post('tasks/store',[TaskController::class, 'store'])->name('task.store')->middleware('auth');
Route::get('tasks/edit/{task}',[TaskController::class, 'edit'])->name('task.edit')->middleware('auth');
Route::put('tasks/update/{task}',[TaskController::class, 'update'])->name('task.update')->middleware('auth');
Route::delete('tasks/delete/{task}',[TaskController::class, 'destroy'])->name('task.delete')->middleware('auth');
Route::get('/show/{task}',[TaskController::class, 'show'])->name('task.show')->middleware('auth');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
