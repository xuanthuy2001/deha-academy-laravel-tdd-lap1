<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTaskRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name'=>'required',
            'content'=>'required'
        ];
    }

    public function  messages()
    {
        return [
            'name.required' => 'khong duoc de trong.',
            'content.required' => 'A message is required',
        ];
    }
}
