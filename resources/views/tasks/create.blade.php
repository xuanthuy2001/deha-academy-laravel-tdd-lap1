
<div class="container p-2">
    <h1>Tasks Form</h1>
    <a href="{{ route('task.index') }}" class="btn btn-success">Task Lists</a>
</div>

<div class="container p-5 my-5 border">
    <form action="{{ route('task.store') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="mb-3 mt-3">
            <label for="name" class="form-label">Email:</label>
            <input type="text" class="form-control" id="email" placeholder="Enter name" name="name">
            @error('name')
            <p class="alert alert-danger mt-2" style="line-height: 1px">{{ $message }}</p>
            @enderror
            h1
        </div>
        <div class="mb-3 mt-3">
            <label for="contents">Comments:</label>
            <textarea class="form-control" rows="5" id="contents" name="contents" placeholder="Enter contents"></textarea>
            @error('contents')
            <p class="alert alert-danger mt-2" style="line-height: 1px">{{ $message }}</p>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
