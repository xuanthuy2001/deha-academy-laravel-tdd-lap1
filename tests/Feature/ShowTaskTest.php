<?php

namespace Tests\Feature;

use App\Models\Task;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ShowTaskTest extends TestCase
{
    /** @test */
    public function authenticated_can_see_task()
    {
        $this->signIn();
        $task = Task::factory()->create()->toArray();

        $response = $this->get($this->RouteTaskShow($task['id']));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.show');
        $response->assertSee($task['name']);
    }

    /** @test */
    public function cant_authenticate_cant_see()
    {
        $taskId = Task::all()->random()->id;
        $response = $this->get($this->RouteTaskShow($taskId));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test */
    public  function  redirect_not_found_if_task_not_exits()
    {
        $this->signIn();
        $response = $this->get($this->RouteTaskShow(-5));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }


}
