<?php

namespace Tests\Feature;

use App\Models\Task;
use Symfony\Component\HttpFoundation\Response ;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
    /** @test */
    public function redirect_login_if_cant_login()
    {
        $response = $this->get($this->RouteGetListTask());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test */
    public function authenticated_can_see_list_task()
    {
        $this->signIn();
        $task = Task::factory()->create();

        $response = $this->get($this->RouteGetListTask());
        $response->assertViewIs('tasks.index');
        $response->assertSuccessful();
        $response->assertSee([$task->name,$task->content]);
    }
}
