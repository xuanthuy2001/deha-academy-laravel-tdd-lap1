<?php

namespace Tests\Feature;

use App\Models\Task;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    /** @test */
    public function authenticated_can_see_form_create()
    {
        $this->signIn();
        $task = Task::all()->random();
        $response = $this->get($this->RouteEditTask($task));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.edit');
        $response->assertSee([$task->name, $task->content]);
    }

    /** @test */
    public function redirect_authenticate_if_not_login()
    {
        $task = Task::all()->random()->id;
        $response = $this->get($this->RouteEditTask($task));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test */
    public function authenticate_can_update_task()
    {
        $this->signIn();
        $task = Task::all()->random()->id;
        $data = Task::factory()->make()->toArray();

        $response = $this->put($this->RouteUpdateTask($task), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('tasks');
        $this->assertDatabaseHas('tasks', $data);
    }

    /** @test */
    public function  get_message_error_if_name_not_exits()
    {
        $this->signIn();
        $task = Task::all()->random()->id;
        $data = Task::factory()->make(['name'=>null])->toArray();

        $response = $this->put($this->RouteUpdateTask($task), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('name');
        $response->assertInvalid(['name' => 'khong duoc de trong.']);
    }

    /** @test */
    public function  get_message_error_if_content_not_exits()
    {
        $this->signIn();
        $task = Task::all()->random()->id;
        $data = Task::factory()->make(['content'=>null])->toArray();

        $response = $this->put($this->RouteUpdateTask($task), $data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('content');
        $response->assertInvalid(['content' => 'A message is required']);
    }

    /** @test */
    public function  if_has_error_display_message_form_update()
    {
        $this->signIn();
        $task = Task::all()->random()->id;
        $data = Task::factory()->make(['content'=>null])->toArray();

        $response = $this->from($this->RouteEditTask($task))->put($this->RouteUpdateTask($task),$data);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('tasks/edit/'.$task);
    }

    /** @test */
    public function  redirect_page_not_found_if_data_not_exits()
    {
        $this->signIn();
        $data = Task::factory()->make()->toArray();

        $response = $this->put($this->RouteUpdateTask(-1),$data);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
