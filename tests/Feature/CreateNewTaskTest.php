<?php

namespace Tests\Feature;

use App\Models\Task;
use Symfony\Component\HttpFoundation\Response ;
use Tests\TestCase;

class CreateNewTaskTest extends TestCase
{
    /** @test */
    public function authenticated_can_see_form_create()
    {
        $this->signIn();
        $response = $this->get($this->RouteTaskCreate());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.create');
    }

    /** @test */
    public function authenticated_can_create_task_new()
    {
        $this->signIn();
        $beforeTask = Task::all()->count();
        $task = Task::factory()->make()->toArray();

        $response = $this->post($this->RouteTaskStore($task));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('tasks');
        $this->assertDatabaseHas('tasks',$task);
        $this->assertCount($beforeTask +1, Task::all());
    }

    /** @test */
    public function can_not_create_task_new_if_name_null()
    {
        $this->signIn();
        $task = Task::factory()->create(['name' => null])->toArray();

        $response = $this->post($this->RouteTaskStore($task));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertInvalid(['name' => 'khong duoc de trong.']);
    }

    /** @test */
    public function can_not_create_task_new_if_content_null()
    {
        $this->signIn();
        $task = Task::factory()->create(['content' => null])->toArray();

        $response = $this->post($this->RouteTaskStore($task));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('content');
    }

    /** @test */
    public function can_not_create_task_new_if_name_and_content_null()
    {
        $this->signIn();
        $task = Task::factory()->create(['name' => null, 'content' => null])->toArray();

        $response = $this->post($this->RouteTaskStore($task));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors(['name', 'content']);
    }

    /** @test */
    public function can_see_validation_error_on_vew()
    {
        $this->signIn();
        $task = Task::factory()->create(['name' => null])->toArray();

        $response = $this->from($this->RouteTaskCreate())->post($this->RouteTaskStore($task));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect($this->RouteTaskCreate());
        $response->assertInvalid([
            'name' => 'khong duoc de trong.'
        ]);
    }


}
