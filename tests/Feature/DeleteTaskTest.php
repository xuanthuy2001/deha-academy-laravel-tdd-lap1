<?php

namespace Tests\Feature;

use App\Models\Task;
use Symfony\Component\HttpFoundation\Response ;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    /** @test */
    public function redirect_login_if_not_authenticate()
    {
        $task = Task::all()->random()->id;

        $response = $this->delete($this->RouteDeleteTask($task));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('login');
    }

    /** @test */
    public function authenticated_can_delete_task()
    {
        $this->signIn();
        $task = Task::factory()->create();
        $before = Task::all()->count();
        $response = $this->delete($this->RouteDeleteTask($task->id));
        $after  = Task::all()->count();
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('tasks');
        $this->assertEquals($before-1,$after);
        $this->assertDeleted($task);
    }

    /** @test */
    public function redirect_page_not_found_if_task_not_exit()
    {
        $this->signIn();
        $response = $this->delete($this->RouteDeleteTask(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}



