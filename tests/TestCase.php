<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function signIn(): TestCase
    {
        return $this->actingAs(User::factory()->create());
    }

    public function RouteGetListTask(): string
    {
        return route('task.index');
    }

    public  function RouteTaskCreate(): string
    {
        return route('task.create');
    }

    public  function RouteTaskStore($data): string
    {
        return route('task.store',$data);
    }

    public function RouteEditTask($id): string
    {
        return route('task.edit',$id);
    }

    public  function RouteUpdateTask($id): string
    {
        return route('task.update',$id);
    }

    public function RouteDeleteTask($id): string
    {
        return route('task.delete', $id);
    }

    public  function RouteTaskShow($data): string
    {
        return route('task.show',$data);
    }
}
